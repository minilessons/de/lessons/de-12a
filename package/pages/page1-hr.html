<p>Digitalne sustave gradimo kako bi obavljali čovjeku koristan posao. Da bi to bilo moguće,
   digitalni sustav mora moći iz okoline prikupljati podatke, obrađivati ih i potom djelovati
   na okolinu.
</p>

<p>
  Dvije su vrste podataka koje digitalni sustav može prikupljati iz okoline:
</p>
<ul>
<li><em>inherentno diskretne podatke</em> - primjerice, je li prozor otvoren ili nije,
    je li pumpa uključena ili nije, je li svjetlo upaljeno ili nije, te</li>
<li><em>inherentno analogne podatke</em> - kolika je temperatura prostorije, koliko je
    osvjetljenje prostorije, koliki je tlak u prostoriji, kolika je brzina vrtnje rotora
    motora, kolika je brzina kretanja vozila i slično.
</li>
</ul>

<p>Ako je u digitalni sustav potrebno unijeti diskretan podatak, to znamo učiniti bez
   ikakvih problema. Ako moramo u digitalni sustav unijeti analognu veličinu - to izravno
   ne možemo učiniti. Naime, da bi digitalni sustav mogao obrađivati takve veličine, iste
   je potrebno reprezentirati na jedini način na koji digitalna računala mogu raditi s
   podatcima: kao niz bitova. Drugim riječima, analognu veličinu potrebno je izmjeriti
   i broj koji se dobije mjerenjem zatim poslati u digitalni sustav. Uređaji koji obavljaju
   pretvorbu analogne veličine u digitalnu ("mjerenje") nazivaju se <em>analogno-digitalni
   pretvornici</em> (AD-pretvornici). Treba napomenuti da najveći dio analogno-digitalnih
   pretvornika (a posebice svi koje ćemo raditi na ovom kolegiju) očekuju da je ulazna 
   veličina ("ono što se mjeri") predstavljena naponom koji se dovodi na ulaz pretvornika.
</p>

<p>Ako želimo mjeriti neku fizikalnu veličinu koja nije napon, tada istu najprije treba 
   pretvoriti u napon čiji je iznos u nekom poznatom odnosu s veličinom koju želimo 
   izmjeriti. Kako ovo ne bi ostalo u zraku, u nastavku slijedi primjer koji nije dio gradiva
   kolegija Digitalna logika, ali lijepo ilustrira jednu konkretnu uporabu. Ako Vas
   konkretni primjer ne zanima ili ste u stisci s vremenom, slobodno ga preskočite.
</p>

<div style="background-color: #eeeeee;">
<div class="mlExampleQ">
<span class="mlExampleKey">Primjer 1.</span>
<span class="mlExampleText">Pogledajmo kako bismo temperaturu prostorije mogli unijeti u
  digitalni sustav.</span>
</div>
<div class="mlExampleA">
<p>
  Za potrebe ovog primjera pretpostavit ćemo da nam je na raspolaganju analogno-digitalni
  pretvornik koji očekuje da je ulazna veličina predstavljena kao napon. Naš je zadatak
  ilustrirati na koji način možemo temperaturu pretvoriti u naponsku veličinu.
</p>
<p>
  Prisjetimo se otpornika. Otpornik je pasivna elektronička komponenta čiji je ohmski otpor
  jedan od važnijih parametara po kojem biramo što nam točno treba. Ove komponente se 
  proizvode na način koji osigurava da je ohmski otpor relativno stabilan, neovisno o 
  temperaturi na kojoj se komponenta nalazi (unutar razumnih granica).
</p>
<p>
  Termistori (<a href="https://en.wikipedia.org/wiki/Thermistor" target="_blank">članak 
  na Wikipediji</a>) su otpornici čiji je otpor puno osjetljiviji na temperaturu i isti se 
  s temperaturom može mijenjati i za nekoliko redova veličine (primjerice od nekoliko 
  stotina kiloohma pa do nekoliko stotina ohma). Ako otpor termistora pada kako temperatura
  raste, govorimo o NTC-termistorima (engl. <i>Negative Temperature Coefficient</i>).
  Ako otpor termistora raste kako temperatura raste, govorimo o PTC-termistorima 
  (engl. <i>Positive Temperature Coefficient</i>).
</p>
<p>
  Pretpostavimo sada da imamo jedan NTC-termistor (R<sub>T</sub> na slici u nastavku) i jedan "običan"
  otpornik (R na slici u nastavku). Uporabom ta dva otpornika možemo izgraditi obično 
  naponsko djelilo, kako je prikazano na slici 1. Napon na izlazu djelila određen je izrazom:
  \[U_{\text{izl}} = U_{\text{REF}}\frac{R_T}{R + R_T}\]
  i funkcija je od temperature na kojoj se nalazi čitav uređaj (odnosno termistor R<sub>T</sub>).
  S porastom temperature, padat će otpor otpornika R<sub>T</sub> a time i izlazni napon U<sub>izl</sub>.
</p>

<figure class="mlFigure">
<img src="@#file#(djelilo1.svg)" alt="Naponsko djelilo"><br>
<figcaption><span class="mlFigKey">Slika 1.</span> naponsko djelilo ostvareno otpornikom i termistrom.</figcaption>
</figure>

<p>
  Pretpostavimo sada da smo sposobni izlazni napon izmjeriti i unijeti ga u digitalni sustav
  (vidi sliku u nastavku).
</p>

<figure class="mlFigure">
<img src="@#file#(djelilo1b.svg)" alt="Naponsko djelilo plus pretvornik"><br>
<figcaption><span class="mlFigKey">Slika 2.</span> naponsko djelilo ostvareno otpornikom i termistrom povezano s AD-pretvornikom.</figcaption>
</figure>

<p>
  Uz pretpostavku da je poznat iznos otpornika R te da raspolažemo iznosom izmjerenog napona,
  iznos otpora otpornika R<sub>T</sub> možemo izračunati prema formuli:
  \[R_T = \frac{U_{\text{izl}}}{U_{\text{REF}}-U_{\text{izl}}} \cdot R.\]
</p>
<p>
  Ako nam je sada poznat izraz koji opisuje na koji se način otpor termistora mijenja s
  temperaturom, prikladnu formulu možemo iskoristiti kako bismo na temelju utvrđenog
  otpora termistora izračunali temperaturu na kojoj se on nalazi.
</p>
<p>
  Implementiramo li ovaj postupak u praksi, dobili smo mogućnost da naš digitalni sustav
  "mjeri" analognu fizikalnu veličinu - temperaturu. Na sličan način moguće je mjeriti
  osvjetljenje (koristimo foto-otpornike) te druge fizikalne veličine: sve ih pretvaramo
  u napon koji potom analogno-digitalni pretvornik mjeri i prevodi u diskretni podatak.
</p>

</div>
</div>

<p>
  Digitalni sustav na svoju okolinu može djelovati na dva načina. Jednostavniji način
  podrazumijeva binarno upravljanje: treba uključiti ili isključiti neki uređaj. U
  složenijem slučaju, digitalni sustav prema okolini treba generirati izlaznu analognu
  veličinu: primjerice, napon čija amplituda utječe na fizikalni proces kojim sustav
  upravlja. Svakako jedan od najpoznatijih primjera su zvučne kartice u računalu koje
  numeričke podatke prevode u amplitudu napona koji se šalje na zvučnike. Brzim promjenama
  generiranih amplituda zvučna kartica na izlazu generira valne oblike koji predstavljaju
  glazbu koju slušamo.
</p>
<p>
  Digitalni uređaji koji na ulazu dobivaju diskretan (binarni) podatak, i koji na izlazu
  generiraju napon čija je amplituda proporcionalna predanoj numeričkoj vrijednosti nazivaju
  se digitalno-analogni pretvornici (DA-pretvornici; engl. <i>Digital-analog converters</i>).
</p>
<p>
  Tipičnu strukturu općenitog digitalnog sustava tada možemo grafički prikazati kao na
  slici 3.
</p>

<figure class="mlFigure">
<img src="@#file#(digsustav.svg)" alt="Digitalni sustav i njegova okolina"><br>
<figcaption><span class="mlFigKey">Slika 3.</span> Digitalni sustav i njegova okolina.</figcaption>
</figure>



