    function DA2RRv1(stages, canvasName) {
      this.stages = stages;
      this.canvas = document.getElementById(canvasName);
      this.context = this.canvas.getContext("2d");
      this.elements = [];
      this.switches = [];
      this.currents = [];

      this.activeRegions = new ActiveRegions();
      connectCanvasActiveRegion(this, this.activeRegions);

      initElements(this);

      this.updateCurrents = function() {
        this.currents[6*this.stages+2].value = 1.0; // Uref/R
        var startCurrent = this.currents[6*this.stages+2].value;
        for(var i = 0; i < this.stages; i++) {
          var sw = this.switches[i];
          var index = 6*i;
          this.currents[index+1].value = startCurrent/2;
          this.currents[index+2].value = startCurrent/2;
          if(sw.isOpen) this.currents[index+3].value = this.currents[index+1].value; else this.currents[index+3].value = 0.0;
          if(sw.isOpen) this.currents[index+4].value = 0.0; else this.currents[index+4].value = this.currents[index+1].value;
          if(i==0) this.currents[6].value = this.currents[4].value; else this.currents[index+6].value = this.currents[index+4].value+this.currents[index].value;
          startCurrent = this.currents[index+1].value;
        }
        for(var i = stages-1; i > 0; i--) {
          var index = 6*i;
          if(i==0) this.currents[index-1].value = this.currents[index+3].value; else this.currents[index-1].value = this.currents[index+3].value+this.currents[index+5].value;
        }
        this.currents[6*this.stages+1].value = this.currents[3].value+this.currents[5].value;
      }

      function initElements(me) {
        var elems = [];

        // Set floating topleft corner...
        var xoff = 20;
        var yoff = 45;
        var branchHeight = 50;
        var beforeSwitch = 30;
        var switchLength = 20;
        var afterSwitch = 30;
        var bridgeWidth = 40;
        var ampConnectorWidth = 20;
        var vbeforeR = 20;
        var vafterR = 20;
        var vafterSwitch = 20;
        var railsDistance = 20;
        var hbeforeR = 20;
        var hafterR = 20;

        var c = [];
        c.push(new flowValue()); // Zero flow
        for(var i=0; i<me.stages; i++) {
          c.push(new flowValue());
          c.push(new flowValue());
          c.push(new flowValue());
          c.push(new flowValue());
          c.push(new flowValue());
          c.push(new flowValue());
        }
        c.push(new flowValue());
        c.push(new flowValue());

        var w1 = new wire(c[1], new xypoint(xoff, yoff));
        w1.advance(0, 30);
        w1.advance(50, 0);
        elems.push(w1);

        var vc = new VoltageConnection(w1.first());
        vc.caption = "+Uref";
        vc.captionPos = 0;
        elems.push(vc);

        var sws = [];

        var rightExit = w1.last();

        var firstRowW = [];
        var firstRowC = [];
        var secondRowW = [];
        var secondRowC = [];
        for(var i=0; i<me.stages; i++) {
          var index = 6*i;
          var w2 = new wire(c[index+1], rightExit);
          w2.advance(0, vbeforeR);
          elems.push(w2);

          var rv = new ResistorV(w2.last());
          rv.caption = "2R";
          elems.push(rv);

          var w3 = new wire(c[index+1], rv.p1);
          w3.advance(0, vafterR);
          elems.push(w3);
          
          var sw1 = new switch2Element();
          sw1.updateTopPosition(w3.last());
          sw1.caption = "a"+(me.stages-1-i);
          elems.push(sw1);
          sws.push(sw1);

          var w4 = new wire(c[index+3], sw1.pbl);
          w4.advance(0, vafterSwitch);
          elems.push(w4);

          firstRowW.push(w4);
          if(i<me.stages-1) firstRowC.push(c[index+5]);

          var w5 = new wire(c[index+4], sw1.pbr);
          w5.advance(0, vafterSwitch+railsDistance);
          elems.push(w5);

          secondRowW.push(w5);
          secondRowC.push(c[index+6]);

          if(i<me.stages-1) {
            var w6 = new wire(c[index+2], rightExit);
            w6.advance(hbeforeR, 0);
            elems.push(w6);

            var rh = new ResistorH(w6.last());
            rh.caption = "R";
            elems.push(rh);

            var w7 = new wire(c[index+2], rh.p1);
            w7.advance(hafterR, 0);
            elems.push(w7);

            rightExit = w7.last();
          } else {
            var w6 = new wire(c[index+2], rightExit);
            w6.advance(50, 0);
            w6.advance(0, vbeforeR);
            elems.push(w6);

            var rv2 = new ResistorV(w6.last());
            rv2.caption = "2R";
            elems.push(rv2);

            var w7 = new wire(c[index+2], rv2.p1);
            w7.advance(0, vafterR);
            elems.push(w7);

            elems.push(new MassConnection(w7.last()));
          }
        }

        for(var i = 0; i < firstRowC.length; i++) {
          var w = new wire(firstRowC[i],firstRowW[i+1].last());
          w.addPoint(firstRowW[i].last());
          elems.push(w);
        }
        var wt1 = new wire(c[me.stages*6+1],firstRowW[0].last());
        wt1.advance(-40,0);
        wt1.advance(0,20);
        elems.push(wt1);
        elems.push(new MassConnection(wt1.last()));
        for(var i = 0; i < secondRowW.length-1; i++) {
          var w = new wire(secondRowC[i],secondRowW[i].last());
          w.addPoint(secondRowW[i+1].last());
          elems.push(w);
        }

        var wt2 = new wire(secondRowC[secondRowC.length-1],secondRowW[secondRowW.length-1].last());
        wt2.advance(secondRowW[1].last().x-secondRowW[0].last().x,0);
        elems.push(wt2);
        
        for(var i = 0; i < sws.length; i++) {
          var sw = sws[i];
          me.activeRegions.add(new ActiveRegion(
            sw.pt.x-sw.switchWidth/2, sw.pt.y,sw.switchWidth,sw.switchHeight,{
              me: me, sw: sw, execute: function() {this.sw.isOpen = !this.sw.isOpen; this.me.updateCurrents();}
            }
          ));
        }

        rightExit = wt2.last();
        var cop = secondRowC[secondRowC.length-1];
        var wb = new wire(cop, rightExit);
        wb.advance(bridgeWidth, 0);
        elems.push(wb);

        var winm = new wire(c[0], wb.last());
        winm.advance(ampConnectorWidth, 0);
        elems.push(winm);

        var op = new OPAmp(winm.last());
        elems.push(op);

        var winp = new wire(c[0], op.pplus);
        winp.advance(-ampConnectorWidth, 0);
        winp.advance(0, op.inpExtra*2);
        elems.push(winp);

        elems.push(new MassConnection(winp.last()));

        var totw = 2*ampConnectorWidth+op.pout.x-op.pmin.x;
        var rf = new ResistorH(new xypoint(0,0));
        rf.caption = "Rf";
        rf.centerTo(new xypoint(wb.last().x+totw/2, wb.last().y-2*op.inpExtra));
        elems.push(rf);

        var wfb1 = new wire(cop, wb.last());
        wfb1.addPoint(new xypoint(wfb1.last().x,rf.p0.y));
        wfb1.addPoint(rf.p0);
        elems.push(wfb1);

        var wfb2 = new wire(cop, rf.p1);
        wfb2.addPoint(new xypoint(op.pout.x+ampConnectorWidth,rf.p1.y));
        wfb2.addPoint(new xypoint(op.pout.x+ampConnectorWidth,op.pout.y));
        wfb2.addPoint(op.pout);
        elems.push(wfb2);

        var wout = new wire(c[0], wfb2.fromEnd(1));
        wout.advance(30,0);
        elems.push(wout);

        elems.push(new ValueLabel(wout.last().relative(2,0), "start", "middle", {
          c: cop, getValue: function() { return (- 8 * this.c.value) + " V"}
        }));

        me.elements = elems;
        me.switches = sws;
        me.currents = c;
      }

      function connectorDot(context, x, y) {
        context.fillStyle = "#000000";
        context.beginPath();
        context.arc(x, y, 3,0,2*Math.PI);
        context.fill();
      }

      this.updateCurrents();

      var lastTime = +Date.now();
      var deltaTime = 0;
      var startTime = lastTime;
      var animTime = 0;
      var me = this;
      setInterval(function() {
        var ctime = +Date.now();
        deltaTime = ctime - lastTime;
        animTime = ctime - startTime;
        lastTime = ctime;
        me.paint();
      }, 40);

      this.paint = function() {
        //console.log("Repaint; animTime = " + animTime +", deltaTime = " + deltaTime);
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.context.textAlign = "start";
        this.context.textBaseline = "alphabetic";

        //this.activeRegions.reset();

        // Set floating topleft corner...
        var xoff = 20;
        var yoff = 10;

        var ctx = this.context;
        this.elements.forEach(function(elem, index) {
          elem.draw(ctx, animTime, deltaTime);
        });
      }

      this.paint();
    }
