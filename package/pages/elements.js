function xypoint(x,y) {
  this.x = x;
  this.y = y;
  this.symbol = 0; // 0...bez prikaza; 1...puni kružić; 2...prazan kružić; 3...strelica gornja
  this.relative = function(dx,dy) {
    return new xypoint(this.x+dx, this.y+dy);
  }
  this.updateTo = function(x,y) {
    this.x = x;
    this.y = y;
  }
  this.updateToPoint = function(p) {
    this.x = p.x;
    this.y = p.y;
  }
  this.draw = function(ctx) {
    if(this.symbol == 0) return;
    if(this.symbol == 1) {
      ctx.fillStyle = "#000000";
      ctx.beginPath();
      ctx.arc(this.x, this.y, 3, 0, 2*Math.PI);
      ctx.fill();
    }
    if(this.symbol == 2) {
      ctx.fillStyle = "#ffffff";
      ctx.beginPath();
      ctx.arc(this.x, this.y, 3, 0, 2*Math.PI);
      ctx.fill();
      ctx.fillStyle = "#000000";
      ctx.beginPath();
      ctx.arc(this.x, this.y, 3, 0, 2*Math.PI);
      ctx.stroke();
    }
  }
}

function flowValue() {
  this.value = 0;
}

function wire(flow,start) {
  this.points = [];
  this.points.push(start);
  this.flowValue = flow;
  this.addPoint = function(p) {
    this.points.push(p);
    return this;
  }
  this.advance = function(dx, dy) {
    var p = this.points[this.points.length-1];
    this.points.push(new xypoint(p.x+dx, p.y+dy));
    return this;
  }
  this.last = function() {
    return this.points[this.points.length-1];
  }
  this.first = function() {
    return this.points[0];
  }
  this.fromEnd = function(i) {
    return this.points[this.points.length-1-i];
  }
  this.fromStart = function(i) {
    return this.points[i];
  }
  this.count = function() {
    return this.points.length;
  }
  this.draw = function(ctx, animTime, deltaTime) {
    var p0 = this.points[0];
    ctx.strokeStyle = "#000000";
    ctx.beginPath();
    ctx.moveTo(p0.x, p0.y);
    for(var index = 1; index < this.points.length; index++) {
      var p1 = this.points[index];
      ctx.lineTo(p1.x, p1.y);
    }
    ctx.stroke();
    if(this.flowValue && this.flowValue.value > 1E-6) {
      ctx.lineWidth = 2;
      ctx.setLineDash([5,10]);
      ctx.lineDashOffset = -((animTime/100) % (5+10));
      p0 = this.points[0];
      ctx.strokeStyle = "#00ff00";
      ctx.beginPath();
      ctx.moveTo(p0.x, p0.y);
      for(var index = 1; index < this.points.length; index++) {
        var p1 = this.points[index];
        ctx.lineTo(p1.x, p1.y);
      }
      ctx.stroke();
      ctx.setLineDash([]);
      ctx.lineDashOffset = 0;
      ctx.lineWidth = 1;
    }
  }
}

function switchElement() {
  this.p0 = new xypoint(0,0);
  this.p1 = new xypoint(0,0);
  this.isOpen = true;
  this.caption = "";

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    if(this.isOpen) {
      var dx = this.p1.x - this.p0.x;
      var dy = this.p1.y - this.p0.y;
      var tx = Math.cos(3.14/10)*dx + Math.sin(3.14/10)*dy;
      var ty = -Math.sin(3.14/10)*dx + Math.cos(3.14/10)*dy;
      ctx.beginPath();
      ctx.moveTo(this.p0.x, this.p0.y);
      ctx.lineTo(this.p0.x+tx, this.p0.y+ty);
      ctx.stroke();
    } else {
      ctx.beginPath();
      ctx.moveTo(this.p0.x, this.p0.y);
      ctx.lineTo(this.p1.x, this.p1.y);
      ctx.stroke();
    }
    ctx.fillStyle = "#000000";
    ctx.beginPath();
    ctx.arc(this.p0.x, this.p0.y, 3,0,2*Math.PI);
    ctx.arc(this.p1.x, this.p1.y, 3,0,2*Math.PI);
    ctx.fill();
    var cap = (this.caption != "" ? this.caption+" = " : "") + (this.isOpen ? "0" : "1");
    ctx.font="12px Georgia";
    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cap, (this.p0.x+this.p1.x)/2, this.p0.y-10);
  }
}

function ResistorH(p0) {
  this.caption = "";
  this.width = 40;
  this.height = 12;
  this.p0 = p0;
  this.p1 = new xypoint(p0.x+this.width,p0.y);

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#ffffff";
    ctx.beginPath();
    ctx.rect(this.p0.x, this.p0.y-this.height/2, this.width,this.height);
    ctx.fill();
    ctx.stroke();
    if(this.caption != "") {
      ctx.font="12px Georgia";
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";
      ctx.fillStyle = "#000000";
      ctx.fillText(this.caption, this.p0.x+this.width/2, this.p0.y-this.height/2-2);
    }
  }

  this.centerTo = function(p) {
    this.p0.updateTo(p.x-this.width/2, p.y);
    this.p1.updateTo(p.x+this.width/2, p.y);
  }
}

function OPAmp(pmin) {
  this.inpGap = 40;
  this.inpExtra = 25;
  this.pmin = pmin;
  this.gain = 1;
  this.gainSet = false;
  this.pplus = new xypoint(pmin.x, pmin.y+this.inpGap);
  this.pout = new xypoint(pmin.x+0.86*(this.inpGap+2*this.inpExtra), pmin.y+this.inpGap/2);

  this.reshape = function(dgap, dextra) {
    this.inpGap += dgap;
    this.inpExtra += dextra;
    this.pplus.updateTo(this.pmin.x, this.pmin.y+this.inpGap);
    this.pout.updateTo(this.pmin.x+0.86*(this.inpGap+2*this.inpExtra), this.pmin.y+this.inpGap/2);
  }
  this.setGain = function(g) {
    this.gainSet = true;
    this.gain = g;
  }
  this.resetGain = function() {
    this.gainSet = false;
    this.gain = 1;
  }
  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#ffffff";
    ctx.beginPath();
    ctx.moveTo(this.pmin.x, this.pmin.y-this.inpExtra);
    ctx.lineTo(this.pplus.x, this.pplus.y+this.inpExtra);
    ctx.lineTo(this.pout.x, this.pout.y);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
    ctx.font="12px Georgia";
    ctx.textAlign = "start";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "#000000";
    ctx.fillText("-", this.pmin.x+2, this.pmin.y);
    ctx.fillText("+", this.pplus.x+2, this.pplus.y);
    ctx.textAlign = "center";
    var g = this.gainSet ? ""+this.gain : String.fromCharCode(0x221e);
    ctx.fillText("Av="+g, (this.pplus.x+this.pout.x)/2, this.pout.y);
  }
}

function MassConnection(p) {
  this.width = 20;
  this.p = p;

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    var lw = ctx.lineWidth;
    ctx.lineWidth = 3;
    ctx.beginPath();
    ctx.moveTo(this.p.x-this.width/2, this.p.y);
    ctx.lineTo(this.p.x+this.width/2, this.p.y);
    ctx.stroke();
    ctx.lineWidth = lw;
  }
}

function VoltageConnection(p) {
  this.width = 10;
  this.p = p;
  this.angle = 3.14/6;
  this.caption = "";
  this.captionPos = 0;  // 0...above; 1...left, 2...right
  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    var lw = ctx.lineWidth;
    var dx = this.width*Math.sin(this.angle);
    var dy = this.width*Math.cos(this.angle);
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.moveTo(this.p.x, this.p.y);
    ctx.lineTo(this.p.x-dx, this.p.y+dy);
    ctx.moveTo(this.p.x, this.p.y);
    ctx.lineTo(this.p.x+dx, this.p.y+dy);
    ctx.stroke();
    ctx.lineWidth = lw;
    if(this.caption!="") {
      ctx.font="12px Georgia";
      ctx.fillStyle = "#000000";
      if(this.captionPos==0) {
        ctx.textAlign = "center";
        ctx.textBaseline = "bottom";
        ctx.fillText(this.caption, this.p.x, this.p.y-2);
      } else if(this.captionPos==1) {
        ctx.textAlign = "end";
        ctx.textBaseline = "top";
        ctx.fillText(this.caption, this.p.x-dx-2, this.p.y);
      } else {
        ctx.textAlign = "start";
        ctx.textBaseline = "top";
        ctx.fillText(this.caption, this.p.x+dx+2, this.p.y);
      }
    }
  }
}

function ValueLabel(p, talign, tbaseline, valueGetter) {
  this.p = p;
  this.talign = talign;
  this.tbaseline = tbaseline;
  this.valueGetter = valueGetter;

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.font="12px Georgia";
    ctx.fillStyle = "#000000";
    ctx.textAlign = talign;
    ctx.textBaseline = tbaseline;
    ctx.fillText(this.valueGetter.getValue(), this.p.x, this.p.y);
  }
}


function switch2Element() {
  this.pt = new xypoint(0,0);
  this.pbl = new xypoint(0,0);
  this.pbr = new xypoint(0,0);
  this.isOpen = true;
  this.caption = "";
  this.switchHeight = 20;
  this.switchWidth = 20;

  this.updateTopPosition = function(p) {
    this.pt.updateTo(p.x,p.y);
    this.pbl.updateTo(p.x-this.switchWidth/2,p.y+this.switchHeight);
    this.pbr.updateTo(p.x+this.switchWidth/2,p.y+this.switchHeight);
  }

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    if(this.isOpen) {
      ctx.beginPath();
      ctx.moveTo(this.pt.x, this.pt.y);
      ctx.lineTo(this.pbl.x, this.pbl.y);
      ctx.stroke();
    } else {
      ctx.beginPath();
      ctx.moveTo(this.pt.x, this.pt.y);
      ctx.lineTo(this.pbr.x, this.pbr.y);
      ctx.stroke();
    }
    ctx.fillStyle = "#000000";
    ctx.beginPath();
    ctx.arc(this.pt.x, this.pt.y, 3,0,2*Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.arc(this.pbl.x, this.pbl.y, 3,0,2*Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.arc(this.pbr.x, this.pbr.y, 3,0,2*Math.PI);
    ctx.fill();
    var cap = (this.caption != "" ? this.caption+" = " : "") + (this.isOpen ? "0" : "1");
    ctx.font="12px Georgia";
    ctx.textAlign = "end";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "#000000";
    ctx.fillText(cap, this.pbl.x, (this.pt.y+this.pbl.y)/2);
  }
}

function ResistorV(p0) {
  this.caption = "";
  this.width = 12;
  this.height = 40;
  this.p0 = p0;
  this.p1 = new xypoint(p0.x,p0.y+this.height);

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#ffffff";
    ctx.beginPath();
    ctx.rect(this.p0.x-this.width/2, this.p0.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();
    if(this.caption != "") {
      ctx.font="12px Georgia";
      ctx.textAlign = "end";
      ctx.textBaseline = "middle";
      ctx.fillStyle = "#000000";
      ctx.fillText(this.caption, this.p0.x-this.width/2-2, this.p0.y+this.height/2);
    }
  }

  this.centerTo = function(p) {
    this.p0.updateTo(p.x, p.y-this.height/2);
    this.p1.updateTo(p.x, p.y+this.height/2);
  }
}

