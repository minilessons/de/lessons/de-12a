<p>
  Kada govorimo o pretvornicima, nužno je razumjeti ograničenja koja proizlaze iz
  činjenice da dolazi do kolizije diskretnog i analognog svijeta. Primjerice, pogledajmo
  jedan analogno-digitalni pretvornik. Njegova zadaća je ulaznu analognu informaciju pretvoriti
  u diskretan (binarni) broj.
</p>
<p>
  Pretpostavimo da je vrijednost ulazne veličine koju pretvornik dobiva ograničena na
  interval [0, U<sub>max</sub>]. Kako se radi o analognoj vrijednosti, različitih napona
  unutar tog intervala ima beskonačno mnogo.
</p>
<p>
  Pogledajmo sada "digitalnu" stranu. Pretvornik treba obaviti mjerenje i zatim izmjerenu
  vrijednost zapisati uporabom <i>n</i> bitova (gdje je <i>n</i> unaprijed dogovoren neki
  fiksni broj koji je karakteristika samog pretvornika). Kako uporabom <i>n</i> bitova
  možemo zapisati samo 2<sup><i>n</i></sup> različitih brojeva, odmah slijedi da ćemo
  pri mjerenju ulaznog podatka i pretvorbi u diskretan zapis unijeti gubitak informacije.
</p>
<p>
  Evo konkretnog primjera. Neka se na ulazu analogno-digitalnog pretvornika mogu javljati
  naponi od 0V do 6V. Neka pretvornik izmjerenu vrijednost zapisuje uporabom 2 bita. Tada
  izlaz pretvornika može biti <code>00</code>, <code>01</code>, <code>10</code> i <code>11</code>.
  Pretpostavimo da <code>00</code> označava da je ulazni napon bio minimalni (0V) a
  <code>11</code> da je bio maksimalni (6V). Za reprezentaciju svih napona između te
  dvije krajnosti ostale su nam samo još dvije binarne kombinacije: <code>01</code> i 
  <code>10</code>, pa ćemo za gotovo svaki ulazni napon sada griješiti. Da bismo minimizirali
  pogrešku koju radimo, idemo napraviti sljedeće: raspon napona od 0V do 6V jednoliko ćemo 
  razdijeliti tako da odaberemo još dva napona (za dvije kodne riječi koje su nam ostale),
  i potom ćemo izmjereni napon kodirati onom kodnom riječi uz koju radimo minimalnu pogrešku
  (pogledajte sliku u nastavku).
</p>

<figure class="mlFigure">
<img src="@#file#(naponi.svg)" alt="Naponska područja"><br>
<figcaption><span class="mlFigKey">Slika 1.</span> naponska područja.</figcaption>
</figure>

<p>
  U skladu s prikazanom slikom, sve ulazne napone od 0V do (bez) 1V kodirat ćemo s <code>00</code>.
  Sve ulazne napone od 1V do (bez) 3V kodirat ćemo s <code>01</code>.
  Sve ulazne napone od 3V do (bez) 5V kodirat ćemo s <code>10</code>.
  Sve ulazne napone od 5V do (bez) 6V kodirat ćemo s <code>11</code>.
  Radimo li tako, nikada nećemo pogriješiti više od pola širine (središnjih) područja.
  Primjerice, ako je ulazni napon 2.99V, kodirat ćemo ga s <code>01</code> (čime smo ga
  proglasili 2V i pogriješili za 0.99V). Ako je ulazni napon 3.01V, kodirat ćemo ga s 
  <code>10</code> (čime smo ga proglasili 4V i pogriješili za 0.99V). Područja oko 2V i
  4V su upravo široka 2V pa je pola područja jednako 1V - maksimalna pogreška koju možemo
  raditi.
</p>

<p>
  Uvedimo sada malo terminologije. Jedan <em>kvant</em> (ili napon kvanta) predstavlja širinu
  naponskog područja unutar kojeg ne razlikujemo napone. Pogledajte sliku 1: kvant iznosi 2V
  jer sve napone od 1V pa do 3V ćemo proglasiti da su 2V - ne razlikujemo ih. Vizualno, kvant
  odgovara razmaku između napona prikazanih s gornje strane slike. Područje širine 6V podijelili
  smo u 2<sup>n</sup>-1 = 2<sup>2</sup>-1 = 3 jednako široka područja, pa je svako upravo širine
  kvanta: 6V/3 = 2V. Općenito kvant računamo prema izrazu:
\[
  U_{\Delta} = \frac{U_{\text{max}}}{2^n-1}
\]
</p>

<p>
  Služimo li se opisanim načinom dodjele "brojeva" ulaznim naponima, maksimalna pogreška
  koju možemo učiniti jest pola širine područja odnosno pola kvanta. Pogreška kvantizacije 
  računa se prema izrazu:
\[
 \epsilon _{\Delta} = \pm \frac{U_{\Delta}}{2}
\]
</p>

<p>
  Uz navedene parametre, još je zgodno zapamtiti <em>rezoluciju</em> pretvornika: ona definira
  koliko postotaka jedan kvant predstavlja u odnosnu na čitavo mjerno područje (manje je bolje).
  Računa se prema izrazu:
\[
  \text{rezolucija} = \frac{U_{\Delta}}{U_{\text{max}}} = \frac{1}{2^n-1}
\]
</p>

<p>
  Razmatramo li digitalno-analogni pretvornik, ulaz tog sklopa je binarna kodna riječ koja
  predstavlja numeričku vrijednost amplitude napona koji sklop treba generirati na izlazu.
  Uočite da se tu radi o broju kvanata - amplituda generiranog napona na izlazu inherentno
  je diskretna (skokovita). Ima li ulazna binarna vrijednost <i>n</i> bitova, sklop omogućava
  generiranje 2<sup><i>n</i></sup> naponskih razina; nije moguće generirati "uistinu" kontinuiranu
  izlaznu vrijednost. Taj se problem može ublažiti tako da se koristi pretvornik koji ima
  dovoljno velik broj bitova (odnosno prikladnu rezoluciju) čime će napon kvanta biti malen.
</p>

<h3>Smanjivanje pogreške</h3>

<p>
  Želimo li smanjiti pogrešku kvantizacije, moramo smanjiti kvant. To možemo učiniti ili 
  tako da smanjimo mjerno područje (ako je to prihvatljivo) ili tako da povećamo <i>n</i>.
  Primjerice, ako analogno-digitalni pretvornik koristi 3-bitne brojeve, tada će veličina
  kvanta biti:
\[
  U_{\Delta} = \frac{6}{2^3-1} = 0.857V
\]
a pogreška kvantizacije:
\[
 \epsilon _{\Delta} = \pm \frac{0.857V}{2} = 0.429V.
\]
Naime, uz 3 bita imamo na raspolaganju 8 brojeva pa interval od 0V do 6V možemo finije pokriti.
Pretvornik koji bi koristio 10 bitova imao bi kvant od svega 5.87mV. 
</p>

<h3>Analogno-digitalni i digitalno-analogni pretvornici</h3>

<p>
  Analogno-digitalni pretvornici često se realiziraju uporabom digitalno-analognih
  pretvornika kao sastavnih komponenata. Stoga ćemo se u nastavku ove minilekcije 
  upoznati s digitalno-analognim pretvornicima dok će više o analogno-digitalnim
  pretvornicima biti riječi na predavanjima.
</p>

<p>
  No prije toga evo pitanja za provjeriti jeste li dobro savladali izneseno gradivo.
</p>


