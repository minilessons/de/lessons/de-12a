    function OpIDvaRb(canvasName,r1,r2,av,u) {
      this.r1 = r1;
      this.r2 = r2;
      this.av = av;
      this.u = u;
      this.canvas = document.getElementById(canvasName);
      this.context = this.canvas.getContext("2d");
      this.elements = [];
      this.currents = [];

      this.activeRegions = new ActiveRegions();
      connectCanvasActiveRegion(this, this.activeRegions);

      initElements(this);

      this.updateCurrents = function() {
      }

      function initElements(me) {
        var elems = [];

        // Set floating topleft corner...
        var xoff = 40;
        var yoff = 45;
        var branchHeight = 50;
        var beforeSwitch = 30;
        var switchLength = 20;
        var afterSwitch = 30;
        var bridgeWidth = 40;
        var ampConnectorWidth = 20;

        var c = [];
        c.push(new flowValue()); // Zero flow
        c.push(new flowValue()); // Current flow
        c.push(new flowValue()); // Voltage flow
        c[1].value = me.u/(me.r1+me.r2);
        c[2].value = me.av<1E-6 ? 25 : -c[1].value*me.r2*me.av;

        var w1 = new wire(c[1], new xypoint(xoff, yoff));
        w1.advance(0, 30);
        elems.push(w1);

        var vc = new VoltageConnection(w1.first());
        vc.caption = "+Ucc = " + me.u + " V";
        vc.captionPos = 0;
        elems.push(vc);

        var sws = [];

        var rightExit = null;

        var w2 = new wire(c[1], w1.last());
        w2.advance(beforeSwitch, 0);

        var r = new ResistorH(w2.last());
        r.caption = "R1 = " + me.r1+"Ω";

        var w4 = new wire(c[1], r.p1);
        w4.advance(beforeSwitch, 0);

        elems.push(w2);
        elems.push(w4);
        elems.push(r);

        rightExit = w4.last();
        
        var wb = new wire(c[1], rightExit);
        wb.advance(bridgeWidth, 0);
        elems.push(wb);

        var winm = new wire(c[0], wb.last());
        winm.advance(ampConnectorWidth, 0);
        elems.push(winm);

        var op = new OPAmp(winm.last());
        if(me.av>=1E-6) op.setGain(me.av);
        op.reshape(20,0);
        elems.push(op);

        var winp = new wire(c[0], op.pplus);
        winp.advance(-ampConnectorWidth, 0);
        winp.advance(0, op.inpExtra*2);
        elems.push(winp);

        elems.push(new MassConnection(winp.last()));

        var totw = 2*ampConnectorWidth+op.pout.x-op.pmin.x;
        var rf = new ResistorH(new xypoint(0,0));
        rf.caption = "Rf = " + me.r2+"Ω";
        rf.centerTo(new xypoint(wb.last().x+totw/2, wb.last().y-2*op.inpExtra));
        elems.push(rf);

        var wfb1 = new wire(c[1], wb.last());
        wfb1.addPoint(new xypoint(wfb1.last().x,rf.p0.y));
        wfb1.addPoint(rf.p0);
        elems.push(wfb1);

        var wfb2 = new wire(c[1], rf.p1);
        wfb2.addPoint(new xypoint(op.pout.x+ampConnectorWidth,rf.p1.y));
        wfb2.addPoint(new xypoint(op.pout.x+ampConnectorWidth,(rf.p1.y+op.pout.y)/2));
        //wfb2.addPoint(op.pout);
        elems.push(wfb2);

        elems.push(new MassConnection(wfb2.last()));

        var wout = new wire(c[0], op.pout);
        wout.advance(30,0);
        elems.push(wout);

        elems.push(new ValueLabel(wout.last().relative(2,0), "start", "middle", {
          c: c[2], getValue: function() { 
            return (Math.round(this.c.value*1000)/1000) + " V";
          }
        }));
        elems.push(new ValueLabel(r.p0.relative((r.p1.x-r.p0.x)/2,10), "center", "top", {
          me: me, c: c[1], getValue: function() { 
            var ur1 = this.c.value*this.me.r1;
            return (Math.round(ur1*1000)/1000) + " V";
          }
        }));
        elems.push(new ValueLabel(rf.p0.relative((rf.p1.x-rf.p0.x)/2,10), "center", "top", {
          me: me, c: c[1], getValue: function() { 
            var ur2 = this.c.value*this.me.r2;
            return (Math.round(ur2*1000)/1000) + " V";
          }
        }));
        elems.push(new ValueLabel(winm.last().relative(-1,1), "end", "top", {
          me: me, c: c[1], getValue: function() { 
            var ur2 = this.c.value*this.me.r2;
            return "Um = " + (Math.round(ur2*1000)/1000) + " V";
          }
        }));
        elems.push(new ValueLabel(winm.last().relative(-1,(winp.first().y-winm.last().y)/2), "end", "middle", {
          me: me, c: c[1], getValue: function() { 
            var ur2 = this.c.value*this.me.r2;
            return "Ud = Up-Um = " + (Math.round(-ur2*1000)/1000) + " V";
          }
        }));
        elems.push(new ValueLabel(winp.first().relative(-1,-1), "end", "bottom", {
          me: me, c: c[1], getValue: function() { 
            return "Up = 0.000 V";
          }
        }));

        me.elements = elems;
        me.currents = c;
      }

      function connectorDot(context, x, y) {
        context.fillStyle = "#000000";
        context.beginPath();
        context.arc(x, y, 3,0,2*Math.PI);
        context.fill();
      }

      var lastTime = +Date.now();
      var deltaTime = 0;
      var startTime = lastTime;
      var animTime = 0;
      var me = this;
      setInterval(function() {
        var ctime = +Date.now();
        deltaTime = ctime - lastTime;
        animTime = ctime - startTime;
        lastTime = ctime;
        me.paint();
      }, 40);

      this.paint = function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.context.textAlign = "start";
        this.context.textBaseline = "alphabetic";

        var ctx = this.context;
        this.elements.forEach(function(elem, index) {
          elem.draw(ctx, animTime, deltaTime);
        });
      }

      this.paint();
    }

