    function TezinskiDA(weights, canvasName) {
      this.weights = weights;
      this.canvas = document.getElementById(canvasName);
      this.context = this.canvas.getContext("2d");
      this.elements = [];
      this.switches = [];
      this.currents = [];

      this.activeRegions = new ActiveRegions();
      connectCanvasActiveRegion(this, this.activeRegions);

      initElements(this);

      this.updateCurrents = function() {
        for(var i = 0; i < this.switches.length; i++) {
          var sw = this.switches[i];
          var w = this.weights[i];
          var index = i < this.switches.length-1 ? 2*i+3 : 2*i;
          var c = this.currents[index];
          if(sw.isOpen) c.value = 0; else c.value = 1.0*w;
        }
        for(var i = this.switches.length-2; i >= 0; i--) {
          if(i>0) {
            this.currents[2*i].value = this.currents[2*i+2].value + this.currents[2*i+3].value;
          } else {
            this.currents[1].value = this.currents[2*i+2].value + this.currents[2*i+3].value;
          }
        }
      }

      function initElements(me) {
        var elems = [];

        // Set floating topleft corner...
        var xoff = 20;
        var yoff = 45;
        var branchHeight = 50;
        var beforeSwitch = 30;
        var switchLength = 20;
        var afterSwitch = 30;
        var bridgeWidth = 40;
        var ampConnectorWidth = 20;

        var c = [];
        c.push(new flowValue()); // Zero flow
        for(var i=0; i<weights.length-1; i++) {
          c.push(new flowValue());  // v-branch
          c.push(new flowValue());  // h-branch
        }
        c.push(new flowValue()); // before-last h-branch

        var w1 = new wire(c[1], new xypoint(xoff, yoff));
        w1.advance(0, 30);
        elems.push(w1);

        var vc = new VoltageConnection(w1.first());
        vc.caption = "+Uref";
        vc.captionPos = 0;
        elems.push(vc);

        var sws = [];

        var rightExit = null;

        for(var i=0; i<weights.length; i++) {
          if(i<weights.length-1) {
            var w2 = new wire(c[(i+1)*2+1], w1.last());
            w2.advance(beforeSwitch, 0);

            var w3 = new wire(c[(i+1)*2+1], w2.last().relative(switchLength, 0));
            w3.advance(afterSwitch, 0);

            var sw = new switchElement();
            sw.caption = "a"+(weights.length-1-i);
            sw.p0.updateToPoint(w2.last());
            sw.p1.updateToPoint(w3.first());
            sws.push(sw);
            elems.push(sw);

            var r = new ResistorH(w3.last());
            r.caption = "R/"+weights[i];

            var w4 = new wire(c[(i+1)*2+1], r.p1);
            w4.advance(beforeSwitch, 0);

            elems.push(w2);
            elems.push(w3);
            elems.push(w4);
            elems.push(r);

            if(i<weights.length-2) {
              var w5 = new wire(c[(i+1)*2], w4.last().relative(0, branchHeight));
              w5.advance(0, -branchHeight);
              elems.push(w5);
              if(i==0) rightExit = w5.last();
            }

          } else if(i==weights.length-1) {
            var w2 = new wire(c[2*i], w1.last());
            w2.advance(0, branchHeight);
            w2.advance(beforeSwitch, 0);

            var w3 = new wire(c[2*i], w2.last().relative(switchLength, 0));
            w3.advance(afterSwitch, 0);

            var sw = new switchElement();
            sw.caption = "a"+(weights.length-1-i);
            sw.p0.updateToPoint(w2.last());
            sw.p1.updateToPoint(w3.first());
            sws.push(sw);
            elems.push(sw);

            var r = new ResistorH(w3.last());
            r.caption = "R/"+weights[i];

            var w4 = new wire(c[2*i], r.p1);
            w4.advance(beforeSwitch, 0);
            w4.advance(0, -branchHeight);

            elems.push(w2);
            elems.push(w3);
            elems.push(w4);
            elems.push(r);
          }
          if(i<weights.length-2) {
            var w = new wire(c[(i+1)*2], w1.last());
            w.advance(0, branchHeight);
            elems.push(w);
            w1 = w;
          }
        }
        
        for(var i = 0; i < sws.length; i++) {
          var sw = sws[i];
          me.activeRegions.add(new ActiveRegion(
            sw.p0.x, sw.p0.y-10,sw.p1.x-sw.p0.x,20,{
              me: me, sw: sw, execute: function() {this.sw.isOpen = !this.sw.isOpen; this.me.updateCurrents();}
            }
          ));
        }

        var wb = new wire(c[1], rightExit);
        wb.advance(bridgeWidth, 0);
        elems.push(wb);

        var winm = new wire(c[0], wb.last());
        winm.advance(ampConnectorWidth, 0);
        elems.push(winm);

        var op = new OPAmp(winm.last());
        elems.push(op);

        var winp = new wire(c[0], op.pplus);
        winp.advance(-ampConnectorWidth, 0);
        winp.advance(0, op.inpExtra*2);
        elems.push(winp);

        elems.push(new MassConnection(winp.last()));

        var totw = 2*ampConnectorWidth+op.pout.x-op.pmin.x;
        var rf = new ResistorH(new xypoint(0,0));
        rf.caption = "Rf";
        rf.centerTo(new xypoint(wb.last().x+totw/2, wb.last().y-2*op.inpExtra));
        elems.push(rf);

        var wfb1 = new wire(c[1], wb.last());
        wfb1.addPoint(new xypoint(wfb1.last().x,rf.p0.y));
        wfb1.addPoint(rf.p0);
        elems.push(wfb1);

        var wfb2 = new wire(c[1], rf.p1);
        wfb2.addPoint(new xypoint(op.pout.x+ampConnectorWidth,rf.p1.y));
        wfb2.addPoint(new xypoint(op.pout.x+ampConnectorWidth,op.pout.y));
        wfb2.addPoint(op.pout);
        elems.push(wfb2);

        var wout = new wire(c[0], wfb2.fromEnd(1));
        wout.advance(30,0);
        elems.push(wout);

        elems.push(new ValueLabel(wout.last().relative(2,0), "start", "middle", {
          c: c[1], getValue: function() { return (- 0.5 * this.c.value) + " V"}
        }));

        me.elements = elems;
        me.switches = sws;
        me.currents = c;
      }

      function connectorDot(context, x, y) {
        context.fillStyle = "#000000";
        context.beginPath();
        context.arc(x, y, 3,0,2*Math.PI);
        context.fill();
      }

      var lastTime = +Date.now();
      var deltaTime = 0;
      var startTime = lastTime;
      var animTime = 0;
      var me = this;
      setInterval(function() {
        var ctime = +Date.now();
        deltaTime = ctime - lastTime;
        animTime = ctime - startTime;
        lastTime = ctime;
        me.paint();
      }, 40);

      this.paint = function() {
        //console.log("Repaint; animTime = " + animTime +", deltaTime = " + deltaTime);
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.context.textAlign = "start";
        this.context.textBaseline = "alphabetic";

        //this.activeRegions.reset();

        // Set floating topleft corner...
        var xoff = 20;
        var yoff = 10;

        var ctx = this.context;
        this.elements.forEach(function(elem, index) {
          elem.draw(ctx, animTime, deltaTime);
        });
      }

      this.paint();
    }

