<p>
  Da bi težinski DA-pretvornik radio ispravno, nužno je osigurati da se u proizvodnji
  otpornici težinske mreže proizvedu na način koji osigurava da njihov omjer vrlo precizno
  odgovara težinama. To u praksi nije lako postići.
</p>

<p>
  DA-pretvornik s ljestvičastom mrežom nastao je kao primjer jednog specifičnog pretvornika
  koji radi isključivo za binarni kod (gdje su težine 2<sup>i</sup>), ali ne pati od problema
  težinskog pretvornika: potrebno je proizvesti samo dvije vrste otpora (točni iznosi uopće
  nisu bitni) i jedino je važno da njihov omjer bude 2 : 1. Iz tog se razloga ovaj pretvornik
  još naziva R-2R pretvornikom.
</p>

<p>
  Da bismo razumjeli njegov rad, najprije se moramo podsjetiti kako se računa otpor
  serijskog i paralelnog spoja. Pogledajmo nekoliko primjera.
</p>

<figure class="mlFigure">
<img src="@#file#(otpori1.svg)" alt="2R || 2R"><br>
<figcaption><span class="mlFigKey">Slika 1.</span> 2R || 2R.</figcaption>
</figure>

<p>
  Slika 1 prikazuje 2 otpornika iznosa 2R koji su spojeni paralelno. Otpor koji se
  vidi s ulaza (lijevi izvod) prema masi stoga je 2R||2R, što je:
\[
  \frac{1}{R_{\text{ekv}}} = \frac{1}{2R} + \frac{1}{2R} = \frac{2}{2R} = \frac{1}{R}
\]
  pa je ekvivalentni otpor jednak:
\[
  R_{\text{ekv}} = R.
\]
  S obzirom da su otpori obaju grana jednaki, struja koja ulazi u ovu mrežu podjednako
  se dijeli pa je pola prolazi kroz lijevu granu a pola kroz desnu. Da smo na ulaz mreže
  spojili izvor napona U, struja I na slici bila bi:
\[
  I = \frac{U}{R}
\]
  dok bi kroz svaku od grana teklo pola tog iznosa:
\[
  I_{\text{grane}} = \frac{1}{2} \cdot \frac{U}{R}
\]
</p>

<p>Pogledajmo sljedeći slučaj.</p>

<figure class="mlFigure">
<img src="@#file#(otpori2.svg)" alt="R + (2R || 2R)"><br>
<figcaption><span class="mlFigKey">Slika 2.</span> R + (2R || 2R).</figcaption>
</figure>

<p>
  Slika 2 prikazuje otpornik iznosa R spojen u seriju s paralelom 2 otpornika iznosa 2R.
  Otpor koji se vidi s ulaza (lijevi izvod) prema masi stoga je R + 2R||2R, što je:
\[
  R_{\text{ekv}} = R + R = 2R.
\]
</p>

<p>Pogledajmo sljedeći slučaj.</p>

<figure class="mlFigure">
<img src="@#file#(otpori3.svg)" alt="2R || (R + (2R || 2R))"><br>
<figcaption><span class="mlFigKey">Slika 3.</span> 2R || (R + (2R || 2R)).</figcaption>
</figure>

<p>
  Slika 3 prikazuje otpornik iznosa 2R kojemu je u paralelu spojena serija otpornika 
  iznosa R serija s paralelom 2 otpornika iznosa 2R.
  Otpor koji se vidi s ulaza (lijevi izvod) prema masi stoga je 2R || (R + 2R||2R), što je:
\[
  R_{\text{ekv}} = R.
\]
  Naime, čitava desna grana (gledano od ulaza) ponaša se kao otpor iznosa 2R, pa ukupno imamo
  ponovno paralelu 2R i 2R što je R.
</p>
<p>
  Da smo na ulaz ove mreže spojili izvor napona U, struja I na slici bila bi opet:
\[
  I = \frac{U}{R}
\]
  kao što je to bio slučaj kod prvog primjera. Kroz svaku od grana izravno vidljivih s ulaza
  teklo bi pola tog iznosa:
\[
  I_{\text{grane1}} = \frac{1}{2} \cdot \frac{U}{R}.
\]
  Ta polovica je struja koja dolazi do drugog dijela mreže i tamo se ponovno dijeli na paraleli
  dvaju otpornika. Stoga kroz svaku od "desnih" grana teče pola od te struje što je:
\[
  I_{\text{grane2}} = \frac{1}{2} \cdot I_{\text{grane1}} = \frac{1}{4} \cdot \frac{U}{R}.
\]
</p>

<p>Pogledajmo posljednji slučaj.</p>

<figure class="mlFigure">
<img src="@#file#(otpori4.svg)" alt="Još složenija mreža"><br>
<figcaption><span class="mlFigKey">Slika 4.</span> Još složenija mreža.</figcaption>
</figure>

<p>
  Slika 4 prikazuje još kompleksniji slučaj, no uočite odmah kakav se otpor vidi gledano s ulaza:
  prema dolje imamo 2R a njemu u paralelu ide kompletna desna grana čiji je ekvivalentni otpor
  ponovno 2R. Time se s ulaza vidi paralela 2R i 2R pa je ekvivalentni otpor čitave mreže:
\[
  R_{\text{ekv}} = R.
\]
</p>

<p>
  Da smo na ulaz ove mreže spojili izvor napona U, struja I na slici bila bi opet:
\[
  I = \frac{U}{R}.
\]
  Sada imamo tri grane: lijevu, srednju i desnu. Kroz svaku od grana izravno vidljivih s ulaza
  teklo bi pola tog iznosa:
\[
  I_{\text{grane1}} = \frac{1}{2} \cdot \frac{U}{R}.
\]
  Ta polovica je struja koja dolazi do središnjeg dijela mreže i tamo se ponovno dijeli na paraleli
  dvaju (jednakih) otpornika. Stoga kroz svaku od "središnjih" grana teče pola od te struje što je:
\[
  I_{\text{grane2}} = \frac{1}{2} \cdot I_{\text{grane1}} = \frac{1}{4} \cdot \frac{U}{R}.
\]
  Struja tog iznosa dolazi i u desni dio mreže gdje se ponovno dijeli:
\[
  I_{\text{grane3}} = \frac{1}{2} \cdot I_{\text{grane2}} = \frac{1}{8} \cdot \frac{U}{R}.
\]
</p>

<p>
  Želimo li proširiti ovakvu mrežu, sve što je potrebno jest s lijeve strane nastaviti dodavati
  2R/R segmente. Ukupni otpor takve mreže, neovisno o broju ovih segmenata uvijek će biti jednak R.
  Ono što je izuzetno korisno svojstvo ove mreže jest da struje koje teku kroz 2R otpore prema masi
  su točno u omjeru 2:1 između dva susjeda (iznimka je sam kraj).
</p>

<p>
  Kako ćemo ovo iskoristiti za izradu pretvornika? Evo sklopa.
</p>

<script>
@#page-include#(elements.js)
@#page-include#(canvasActRegUtil.js)
@#page-include#(da2rrv1.js)
</script>

<div style="width: 630px; height: 400px; text-align: center;">
<canvas id="myCanvas1" width="630" height="400"></canvas>
</div>

<script>
  new DA2RRv1(4,"myCanvas1");
</script>

<p>
  Pogledajte dobro prikazanu shemu. Konstruirali smo 2R-R mrežu gdje su svi 2R otpornici
  i dalje spojeni na masu; razlika je sada samo u tome što je taj spoj ostvaren kroz sklopku
  koja struju šalje ili u "pravu" masu ili u "virtualnu" masu koju održava diferencijsko
  pojačalo.
</p>

<p>
  Pogledajte granu u kojoj se nalazi sklopa a0. Kroz nju teče struja:
\[
  I_0 = \frac{U_{\text{REF}}}{R} \frac{1}{16}.
\]
  Označimo tu struju s \(I^{*}\). Dakle:
\[
  I_0 = 1 \cdot I^{*}.
\]
  Kroz granu u kojoj se nalazi sklopka a1 teče struja:
\[
  I_1 = \frac{U_{\text{REF}}}{R} \frac{1}{8} = 2 \cdot I^{*}.
\]
  Kroz granu u kojoj se nalazi sklopka a2 teče struja:
\[
  I_2 = \frac{U_{\text{REF}}}{R} \frac{1}{4} = 4 \cdot I^{*}.
\]
  Kroz granu u kojoj se nalazi sklopka a3 teče struja:
\[
  I_3 = \frac{U_{\text{REF}}}{R} \frac{1}{2} = 8 \cdot I^{*}.
\]
</p>
<p>
  Ako ništa niste dirali, sklopke su podešene tako da sve ove struje šalju u "pravu"
  masu. Što se događa ako neku od sklopki prebacimo u stanje 1? Prebacite a0 u stanje 1. 
  Struja te grane
  poteći će prema operacijskom pojačalu. Ukupna struja koja umjesto u pravu masu teče 
  prema operacijskom pojačalu bit će određena izrazom:
\[
  I_N = a_3 \cdot I_3 + a_2 \cdot I_2 + a_1 \cdot I_1 + a_0 \cdot I_0
\]
  što dalje možemo zapisati kao:
\[
\begin{align}
  I_N &amp;= a_3 \cdot 8 \cdot I^{*} + a_2 \cdot 4 \cdot I^{*} + a_1 \cdot 2 \cdot I^{*} + a_0 \cdot 1 \cdot I^{*} \\
      &amp;= I^{*} \cdot (a_3 \cdot 8 + a_2 \cdot 4 + a_1 \cdot 2 + a_0 \cdot 1) \\
      &amp;= I^{*} \cdot \sum_{i=0}^{3} a_i \cdot 2^i \\
      &amp;= I^{*} \cdot N.
\end{align}
\]
  Već smo naučili da ta struja prolazi direktno kroz otpornik Rf pa je izlazni napon
  sklopa jednak:
\[
  U_{\text{izl}} = - I_N \cdot R_f = -I^{*} \cdot N \cdot R_f = -\frac{U_{\text{REF}}}{16} \frac{R_f}{R} \cdot N.
\]
Ako za \(R_f\) odaberemo zgodan iznos 2R, dobit ćemo konačan izraz:
\[
  U_{\text{izl}} = -\frac{U_{\text{REF}}}{16} \frac{2R}{R} \cdot N = -\frac{U_{\text{REF}}}{8} \cdot N.
\]
Razmatramo li općenitu mrežu koja ima <i>n</i> sklopki, kod nje će umjesto broja 16 u prethodnim izrazima 
pisati \(2^n\) pa u tom slučaju dobivamo uz \(R_f = 2R\) izraz:
\[
  U_{\text{izl}} = -\frac{U_{\text{REF}}}{2^{n-1}} \cdot N.
\]
i taj je izraz dan i u službenoj prezentaciji na predavanjima.
</p>

<div class="mlNote">
<p>
  Opisana 2R-R mreža može se koristiti na različite načine kako bi se dobio prikladan DA-pretvornik.
  Implementacija kod koje se na ulaz mreže dovodi fiksan napon a sklopke struje grana preklapaju između
  prave i virtualne mase, samo je jedna od mogućih.
</p>
<p>
  U prezentaciji uz predavanja dani su prikazi i drugačijih implementacija - primjerice implementacije
  kod koje sklopke u granama dovode ili 0V ili referentni napon. Kod takve izvedbe, "pretvoreni" se napon
  dobiva na ulazu mreže (koji tada koristimo kao izlaz).
</p>
</div>

